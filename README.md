
# Large Core

[![npm version](https://badge.fury.io/js/large-core.svg)](https://badge.fury.io/js/large-core)


![image](https://raw.githubusercontent.com/ptoner/Large/master/www/images/logo_white.png)

**Large Core** is a library that helps you built P2P Typescript apps using [IPFS](https://github.com/ipfs/ipfs), [OrbitDB](https://github.com/orbitdb/orbit-db), and [Ethereum](https://github.com/ethereum/solidity). 

It provides:

* A configured OrbitDB instance.

* An [ethers.js](https://github.com/ethers-io/ethers.js/) Ethereum wallet.

* Pre-built data services

    * **Profile Service** - Each wallet has a user profile. Use the default or expand it with other fields.
    * **Friend Service** - Follow other users. Automatically get updates to their posts and profiles.
    * **Read Only Post Service** - Load a large feed of posts and easily page through it. It's very lazily-loaded. Posts can not be edited.
    * **Blog Post Service** - A post service that allows for posts to be updated. Can still be paged through without loading all of the content. Loads a bit more data than the read only post service.
    * **Page Service** - Manage static pages. 
    * **Group Service** - Not available yet. Will manage groups.
    * **Schema Service** - Each Large instance has a schema that it adheres to. 
    * **Wallet Service** - Used in cases where MetaMask is not available.
    * **Site Settings Service** - Basic info about the website. 
    * **Feed Monitor Service** - Manages subscriptions to various post feeds. Listens for updates to all feeds and puts any posts it finds into the user's main feed.

* Works in the browser, Electron, and (soon) Android/iOS.
    * Use MetaMask or a built-in wallet instead. 

* Data is stored in IPFS and OrbitDB. Right now all data is unencrypted. This will likely change.

* Ethereum is used to authenticate messages. 



# Install

Include as a dependency in your package.json

```javascript
  "dependencies": {
    "large-core": "0.0.7"
  }   
```

# Usage

```javascript

    import Core from 'large-core'


    //Initialize. It will need to be inside an async function
    try {
        await Core.initialize()
    } catch(ex) {
        console.log(ex)
    }
    


    //Access services
    let walletService:WalletService = Core.walletService
    let processFeedService:ProcessFeedService = Core.processFeedService
    let profileService:ProfileService = Core.profileService
    let postService:PostService = Core.postService
    let friendService:FriendService = Core.friendService
    let schemaService:SchemaService = Core.schemaService
    let identityService:IdentityService = Core.identityService
    let imageService:ImageService = Core.imageService

```



# Events

updated-unread-posts //TODO: Add detail