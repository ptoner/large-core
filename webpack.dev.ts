import { merge } from 'webpack-merge'
import path from 'path'
import common from './webpack.common'


export default merge(common, {
    //@ts-ignore
    mode: 'development',
    devtool: 'source-map',
    watch: true
})
