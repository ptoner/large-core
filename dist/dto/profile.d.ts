declare class Profile {
    _id?: string;
    owner?: string;
    name?: string;
    aboutMe?: string;
    profilePic?: string;
    lastKnownAddress?: string[];
    following?: boolean;
}
export { Profile };
