interface SiteSettings {
    title?: string;
    tagline?: string;
    publicEmailAddress?: string;
    timezone?: string;
}
export { SiteSettings };
