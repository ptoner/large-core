declare class Friend {
    address?: string;
    lastPostId?: string;
    lastPostMilli?: number;
    lastKnownAddress?: string[];
    _id?: string;
    _rev?: string;
}
export { Friend };
