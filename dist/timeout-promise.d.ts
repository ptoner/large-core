declare function timeout(value: number): any;
declare const timeout_fn: (ms: number, promise: any) => Promise<any>;
export { timeout, timeout_fn };
