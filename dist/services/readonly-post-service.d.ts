import { Post } from "../dto/post";
import { SchemaService } from "./schema-service";
import { PostService } from "./post-service";
import { OrbitService } from "./orbit-service";
declare class ReadOnlyPostService {
    private schemaService;
    private postService;
    private orbitService;
    feedStore: any;
    constructor(schemaService: SchemaService, postService: PostService, orbitService: OrbitService);
    loadPostFeedForWallet(walletAddress: string): Promise<void>;
    loadMainFeedForWallet(walletAddress: string): Promise<void>;
    loadRepliesFeed(feedAddress: string): Promise<any>;
    getPosts(limit: number, offset: number, startKey?: string, endKey?: string): Promise<any>;
    put(post: Post): Promise<Post>;
    get(key: string): Promise<Post>;
    delete(post: Post): Promise<void>;
    postMessage(content: any, walletAddress: string): Promise<Post>;
    postReply(parent: Post, content: any, walletAddress: string): Promise<Post>;
    translatePost(post: Post): Promise<Post>;
    load(): Promise<any>;
    close(): Promise<any>;
}
export { ReadOnlyPostService };
