declare class WalletService {
    private provider;
    wallet: any;
    constructor(provider: any);
    initWallet(): Promise<void>;
    connect(): Promise<void>;
    getAddress(): Promise<any>;
}
export { WalletService };
