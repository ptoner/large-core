import { SchemaService } from "./schema-service";
import { SiteSettings } from "../dto/site-settings";
declare class SiteSettingsService {
    private schemaService;
    loadedWalletAddress: string;
    siteSettingsStore: any;
    constructor(schemaService: SchemaService);
    loadStoreForWallet(walletAddress: string): Promise<void>;
    put(walletAddress: string, settings: SiteSettings): Promise<SiteSettings>;
    get(walletAddress: string): Promise<SiteSettings>;
    delete(walletAddress: string): Promise<void>;
    close(): Promise<void>;
}
export { SiteSettingsService };
