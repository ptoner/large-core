import { Profile } from "../dto/profile";
import { SchemaService } from "./schema-service";
import { OrbitService } from "./orbit-service";
declare class ProfileService {
    private schemaService;
    private orbitService;
    profileStore: any;
    constructor(schemaService: SchemaService, orbitService: OrbitService);
    getCurrentUser(): Promise<Profile>;
    getProfileByWallet(walletAddress: string): Promise<Profile>;
    updateLastKnownAddress(): Promise<void>;
    loadStoreForWallet(walletAddress: string): Promise<void>;
    get(key: string): Promise<Profile>;
    put(profile: Profile): Promise<Profile>;
    load(): Promise<any>;
}
export { ProfileService };
