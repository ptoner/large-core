import { ReadOnlyPostService } from "./readonly-post-service";
import { Post } from "../dto/post";
import { FriendService } from "./friend-service";
import { Friend } from "../dto/friend";
import { SchemaService } from "./schema-service";
import { OrbitService } from "./orbit-service";
declare class FeedMonitorService {
    private schemaService;
    private friendService;
    private orbitService;
    private readOnlyPostService;
    private eventEmitter;
    private _unreadPosts;
    getUnreadPosts(): number;
    constructor(schemaService: SchemaService, friendService: FriendService, orbitService: OrbitService, readOnlyPostService: ReadOnlyPostService, eventEmitter: any);
    start(): Promise<void>;
    getFriendsNewPosts(walletAddress: string): Promise<void>;
    getNewPostsFromFriend(friend: Friend): Promise<Post[]>;
    saveFriendPosts(posts: Post[], friends: Friend[], walletAddress: string): Promise<void>;
    updateFriendLastPost(friend: Friend, post: Post): Promise<any>;
    markAllPostsRead(): Promise<void>;
}
export { FeedMonitorService };
