import { ProfileService } from "./profile-service";
import { SchemaService } from "./schema-service";
import { Friend } from "../dto/friend";
import { OrbitService } from "./orbit-service";
declare class FriendService {
    private schemaService;
    private profileService;
    private orbitService;
    friendStore: any;
    constructor(schemaService: SchemaService, profileService: ProfileService, orbitService: OrbitService);
    loadStoreForWallet(walletAddress: string): Promise<void>;
    connectToFriends(walletAddress: string): Promise<void>;
    get(address: string): Promise<any>;
    put(friend: Friend): Promise<void>;
    delete(address: string): Promise<any>;
    getFriends(limit: number, offset: number, startKey?: string, endKey?: string): Promise<any>;
    listByDate(): Promise<Friend[]>;
    getAll(): Promise<Friend[]>;
    close(): Promise<any>;
    load(amount?: number): Promise<any>;
    updateFriendLastKnownAddress(friend: Friend): Promise<void>;
    connect(friend: Friend): Promise<void>;
}
export { FriendService };
