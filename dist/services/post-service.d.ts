import { Post } from "../dto/post";
import { ImageService } from "./image-service";
import { SchemaService } from "./schema-service";
import { ProfileService } from "./profile-service";
declare class PostService {
    private schemaService;
    private imageService;
    private profileService;
    constructor(schemaService: SchemaService, imageService: ImageService, profileService: ProfileService);
    buildPost(walletAddress: string, content: any, parent?: Post): Promise<Post>;
    translatePost(post: Post): Promise<Post>;
    translateContent(post: any): Promise<string>;
}
export { PostService };
