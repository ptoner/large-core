import { SchemaService } from "./schema-service";
import { Page } from "../dto/page";
import { PostService } from "./post-service";
declare class PageService {
    private schemaService;
    private postService;
    loadedWalletAddress: string;
    pageStore: any;
    constructor(schemaService: SchemaService, postService: PostService);
    loadStoreForWallet(walletAddress: string): Promise<void>;
    getPages(): Promise<Page[]>;
    resetHomePage(): Promise<void>;
    put(page: Page): Promise<Page>;
    get(key: string): Promise<Page>;
    delete(key: string): Promise<void>;
    translatePage(page: Page): Promise<Page>;
    close(): Promise<void>;
}
export { PageService };
