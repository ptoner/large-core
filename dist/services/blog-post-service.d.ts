import { SchemaService } from "./schema-service";
import { PostService } from "./post-service";
import { BlogPost } from "../dto/blog-post";
declare class BlogPostService {
    private schemaService;
    private postService;
    loadedWalletAddress: string;
    blogPostStore: any;
    pagingOptions: any;
    constructor(schemaService: SchemaService, postService: PostService);
    loadStoreForWallet(walletAddress: string): Promise<void>;
    getPosts(limit: number, offset: number, startKey?: string, endKey?: string): Promise<BlogPost[]>;
    put(post: BlogPost): Promise<BlogPost>;
    get(key: string): Promise<BlogPost>;
    delete(key: string): Promise<void>;
    load(amount: any): Promise<void>;
    close(): Promise<void>;
}
export { BlogPostService };
