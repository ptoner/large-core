import { WalletService } from "./wallet-service";
declare class OrbitService {
    private walletService;
    private ipfsOptions;
    private orbitOptions;
    orbitDb: any;
    ipfs: any;
    private identity;
    constructor(walletService: WalletService, ipfsOptions: any, orbitOptions: any);
    init(askIdentity?: boolean): Promise<void>;
    getIdentity(keystore: any): Promise<any>;
    getPublicAccessController(orbitdb: any): {
        type: string;
        write: string[];
    };
    getPrivateAccessController(walletAddress: string): {
        type: string;
        write: string[];
    };
}
export { OrbitService };
