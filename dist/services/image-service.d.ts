import { OrbitService } from "./orbit-service";
declare class ImageService {
    private orbitService;
    constructor(orbitService: OrbitService);
    cidToUrl(cid: string): Promise<string>;
}
export { ImageService };
