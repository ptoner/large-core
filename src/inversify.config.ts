import { Container } from "inversify";
import { providers } from "ethers"
import { SchemaService } from "./services/schema-service";
import { WalletService } from "./services/wallet-service";
import EventEmitter from "events";
import { ImageService } from "./services/image-service";
import { ProfileService } from "./services/profile-service";
import { PostService } from "./services/post-service";
import { BlogPostService } from "./services/blog-post-service";
import { ReadOnlyPostService } from "./services/readonly-post-service";
import { PageService } from "./services/page-service";
import { FriendService } from "./services/friend-service";
import { FeedMonitorService } from "./services/feed-monitor-service";
import { OrbitService } from "./services/orbit-service";
import { SiteSettingsService } from "./services/site-settings-service";




let container

function getContainer() {

    if (container) return container

    container = new Container()

    // function provider() {

    //     if (typeof window !== "undefined" && window['ethereum']) {
    
    //         //@ts-ignore
    //         window.web3Provider = window.ethereum
      
    //         //@ts-ignore
    //         return new providers.Web3Provider(web3.currentProvider)  
      
    //     } else {
    //         return providers.getDefaultProvider()
    //     }   
    // }

    function eventEmitter() {
        return new EventEmitter()
    }

    // container.bind("provider").toConstantValue(provider())
    container.bind("eventEmitter").toConstantValue(eventEmitter())

    container.bind(SchemaService).toSelf().inSingletonScope()
    container.bind(WalletService).toSelf().inSingletonScope()
    container.bind(ImageService).toSelf().inSingletonScope()
    container.bind(ProfileService).toSelf().inSingletonScope()
    container.bind(PostService).toSelf().inSingletonScope()
    container.bind(BlogPostService).toSelf().inSingletonScope()
    container.bind(ReadOnlyPostService).toSelf().inSingletonScope()
    container.bind(PageService).toSelf().inSingletonScope()
    container.bind(FriendService).toSelf().inSingletonScope()
    container.bind(FeedMonitorService).toSelf().inSingletonScope()
    container.bind(OrbitService).toSelf().inSingletonScope()
    container.bind(SiteSettingsService).toSelf().inSingletonScope()

    return container
}




export {
    getContainer
}